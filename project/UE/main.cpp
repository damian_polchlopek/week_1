#include "Application.hpp"
#include "ApplicationEnvironmentFactory.hpp"

int main(int argc, char* argv[])
{
    using namespace ue;

    auto appEnv = ue::createApplicationEnvironment(argc, argv);
    auto& logger = appEnv->getLogger();
    auto phoneNumber = appEnv->getMyPhoneNumber();

    Application appUnused; // Unused suffix added to prevent from compiler warning, TODO - remove when used...

    logger.logInfo("Started: ", phoneNumber);
    appEnv->startMessageLoop();
    logger.logInfo("Stopped: ", phoneNumber);
}

