#include "IUeConnectionFactoryMock.hpp"

namespace bts
{

IUeConnectionFactoryMock::IUeConnectionFactoryMock()
{}

IUeConnectionFactoryMock::~IUeConnectionFactoryMock()
{}

IUeRelay::UePtr IUeConnectionFactoryMock::createConnection(ITransportPtr transport)
{
    return IUeRelay::UePtr(createConnectionMock(transport));
}

}
