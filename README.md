<<<<<<< HEAD
# Exercises

## Exercise 1 - do Attach without timeouts. Use proper loggers (Prefixed loggers) to log.

Tests are not required (since students did have unit testing lecture yet) - but, of course - they are welcome.

### Current situation of project

We have class ue::Application that does nothing.

Assumption:

All code will be placed in class Application.

Read project specification (chapter 1.1 and chapter 2) and read the COMMON and UE/ApplicationEnvironment to get familiar
with already implemented part of project.

Do not hesitate to ask trainer.

### Steps

The steps below are just proposal how this can be implemented.

### Step1: Add logger to application and UE phone number to application.

This is just moving things from main() function to ue::Application class.

Student must remember to add ILoggerMock to ApplicationTestSuite to make it compile (use NiceMock<ILoggerMock> so tests passes!)
 
### Step2: Start receiving the messages

Inject (pass via reference) common::ITransport to ue::Application.

Start observing incomming messages by using ITransport::registerMessageCallback

Class to use common::IncomingMessage.

### Step3: Start recognizing the SIB messages and sends AttachRequest

Add proper code to function that recognizes the incoming messages to recognize SIB.
If so - sends AttachRequest.

See specification chapters:

* 3.2.1 SIB Message
* 3.2.2 Attach Messages


### Step4: Start recognizing the AttachResponse, Do not send AttachRequest on SIB when received AttachResponse(accepted:true)

Add proper code to function that handles SIB.

Store Applicate "State" - if it is waiting for AttachResponse(Connecting) or for Sib (NotConnected) or received AttachResponse(true) - Connected

The Applicate "State" might be implemented as enum type.

See specification chapters:

* 4.1.1 Attach to BTS

but ignore timeout scenarios of Attach.

#### Verification Conctept of Step4:

From BTS - issue the following when you see this log:

    #19,tid:1:[UE::xxxxx:123:A]Attached

    t send Sib 0 123 0x

    t send AttachResponse 0 123 0x01

    t send AttachResponse 0 123 0x00


In respone you should see these(or similar) logs in UE(123):

    #11,tid:1[ERROR]: [phone:123][APP:Connected] Unexpected SIB
    #13,tid:1[ERROR]: [phone:123][APP:Connected] Unexpected Attach Accept
    #15,tid:1[ERROR]: [phone:123][APP:Connected] Unexpected Attach Reject



## Exercise 2 - do receive SMS-es, add main menu, refactor

Tests are not required (since students did have unit testing lecture yet) - but, of course - they are welcome.

### Goals

The goal is to implement:

* SMS DB
* Receiving SMSs
* Encapsulate Bts Sending and Receiving sides
* Add GUI support for showing PhoneNumber, states: NotConnected, Connecting, Main Menu

### Ex2 Step 1 - create BtsPort, BtsSender classes

Extract code from ue::Application that is responsible for dealing with UE-BTS messages.

It could be:

* class BtsPort shall talk to Application (via IBtsReceiver interface)
* class Application shall talk to BtsSender (via IBtsSender interface)

### Ex2 Step 2 - create SmsDB, add handling of Sms message

class SmsDB shall have Sms-es

Single Sms represents:

* SMS text
* Phone number (from/to)
* time of receiving (use <chrono>)

BtsPort and Application shall handle Sms from Bts

Read chapters

* 3.3.3 SMS message
* 3.3.2.1 No encryption
* 4.1.3 Receiving SMS

#### Verification Concept of Step2:

From BTS - issue the following when you see this log:

    t send Sms 22 123 0x00323334353637
    t send Sms 33 123 0x0037363534

and see logs from UE

### Ex2 Step 3 - Add GUI to show NotConnected, Connecting, MainMenu

Create interface/class IUser/User that encapsulates the IUeGui for Application.

Move BTS and User related ports to dedicated directories (UE/Application/Bts UE/Application/User)

## Exercise 3 - add Unit Tests to existing classes

The goal of this exercise is to add unit tests for existing classes.
It might happend that UT guards you to make some changes in project - like fixing bugs or simplify design.
But the assumption is - that we do not add any new functionality to the project in this exercise.

### Ex3 Step 1 - UT for Sms classes

Classes SmsDBTestSuite (maybe SmsTestSuite if Sms is separate class/struct) - no Mocks needed (just NiceMock<common::ILoggerMock>)

### Ex3 Step 2 - UT for Bts classes

Classes BtsPortTestSuite, BtsSenderTestSuite

### Ex3 Step 3 - UT for User classes

Classes UserTestSuite, Mock for UeGui and ListViewMode needed

### Ex3 Step 4 - UT for Application

Classes ApplicationTestSuite

This is last step.

## Exercise 4 - use Design Patterns  - like States - refactoring exercise - UT shall guard you

We do not add any new functionality to the project in this exercise.
This is pure refactoring.

### Ex4 Step 1 - Define Application State

Encapsulate each state of application in a separate classes. Follow State Design Pattern.

Proposed classes:

* NotConnectedState, ConnectingState, ConnectedState
* BaseState - as a base classes for the classes above (to implement all ignore event methods)
* StateContext - struct that shall keep all necessary data for states (like ITransport&, ILogger&, IBtsSender&, IUser&, ISmsDB&...)
* Combine interface for events handling (IEventsReceiver - the interface for state) - so Application and State can have same interface

### Ex4 Step 2 - Define Factory for Application States

Class StateFactory

Follow Factory Design Pattern.

### Ex4 Step 3 - Define Locator for Application States

Class StateLocator

The class is necessary to guard the lifetime of the current State. This could be done by Application - but better is to keep such functionality in separate class.

## Exercise 5 - add Sms Send and View (TDD)


Read chapters:

* 4.1.4 Viewing SMS
* 4.1.5 Sending SMS

Try to make this exercise with following Test Driven Development - first write tests.

Try to implement first Application(States) classes. Then all needed utility classes like BtsPort/BtsSender/User/SmsDB

### Ex5 Step 1 - Correct menu - add View Sms and Send Sms - define states for it

Proposed new classes:

* SendSmsState
* ViewSmsesState
* ViewSmsState
* IUserReceiver - new interface to interact from User class to Application/State classes

### Ex5 Step 2 - Make view SMS-es work

Update class User, use/update ViewSmsesState.

### Ex5 Step 3 - Get back from view SMS-es state

Update related classes, add ExitView event.

### Ex5 Step 4 - View Sms

Update related classes.

### Ex5 Step 5 - Edit Sms

Update related classes.


## Exercise 6 - implement Call scenarios (TDD)

Try to make this exercise with following Test Driven Development - first write tests.

Try to implement first Application(States) classes. Then all needed utility classes like BtsPort/BtsSender/User...

At this point we assume that student is pretty much aware of how to divide exercise into implementation steps,
so only the related chapters provided to implement:

* 4.1.6 Receiving Call Request (Call initiated by peer user)
* 4.1.7 Sending Call Request (Call initiated by user)
* 4.1.8 Dropping Call (either initiated by user or peer user)
* 4.1.9 Call (talking)

We assume that the timeout part of scenarios will not be implemented here.

Implementation hints (possible new state classes):

* Receiving Call Request: PeerCallRequestState
* Sending Call Request: DialState, CallRequestState
* Dropping Call: this is stateless action
* Call: TalkingState

## Exercise 7 - implement all timeout scenarios (TDD)

The goal is to implement all timeout scenarios for already implemented use cases (exercises).

Proposed steps as follow:

### Ex7 Step1 - change Application and its States - add timers/timeout events

Define interfaces/classes:

* ITimeoutEvents - as a base for Application and Application States - to receive all necessary timeour events
* ITimers - as interface used by Application States to start/stop timers
* Introduce class Timers implementing ITimers - that can be simple stub class (do nothing)

Having the above - implement all missing scenarios on Application States  level (TDD).

### Ex7 Step2 - prepare Application for multithread environment

Since we implement Timer as separate thread - then it will be necessary for Application class to be prepared for events from two sources (threads)

* already used main thread (ITransport, IUeGui)
* the timeout events (ITimeoutEventsReceiver)

This can be done with Decorator Design PAttern - so we create SynchronizedApplication that will pass all events to its decorated application
with using of <mutex> library.

Remeber to test such SynchronizedApplication class.

### Ex7 Step3 - implement Timers class

TImers class shall use <thread>, <mutex> or <atomic> <chrono> standard libraries to implement new thread that will:

* implement ITimers interface - so for each timer possible used by Application it should give the application possibility to start and stop that timer
* when timer expires - it shall call Application via ITimeoutEventsReceiver interface

Remember that to properly test such Timers class - we have to "mock" its time source,
in order to not wait in our UT e.g. 5 minutes to get know if class is working or not.

To do that - e.g. student can introduce "Tick" property.

## Exercise 8 - implement encryption (TDD)

Modify Call and Sms use cases (see previous exercises) in a way that is possible to encrypt ocmmunication.

See chapters:

* 3.2. Encryption

We have already linked OpenSsh library to our project.

We use this library for RSA encryption. 
RSA ecnryption is not "symetric" so possibility of asymetric encryption should be taken into accound when implementing this exercise.

Definition of Done specific to this exercise:

* Already implemented can be modified very slightly
* The encryption/decryption mechanism shall be separated from other code (via interfaces)
* The information what kind of enncryption is used (e.g. Ceaser enryption or RSA) shall be othogonal to existing scenarios
* "No encryption" should be treaded in a same way as other kind of enryption

## Exercise 9 - implement all use cases interactions (TDD)

Implement chapter 4.2 UE basic interactions

To do that we must start to observe Close Application event (see IUeGui interface).

=======
**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

When you're done, you can delete the content in this README and update the file with details for others getting started with your repository.

*We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket.*

---

## Edit a file

You’ll start by editing this README file to learn how to edit a file in Bitbucket.

1. Click **Source** on the left side.
2. Click the README.md link from the list of files.
3. Click the **Edit** button.
4. Delete the following text: *Delete this line to make a change to the README from Bitbucket.*
5. After making your change, click **Commit** and then **Commit** again in the dialog. The commit page will open and you’ll see the change you just made.
6. Go back to the **Source** page.

---

## Create a file

Next, you’ll add a new file to this repository.

1. Click the **New file** button at the top of the **Source** page.
2. Give the file a filename of **contributors.txt**.
3. Enter your name in the empty file space.
4. Click **Commit** and then **Commit** again in the dialog.
5. Go back to the **Source** page.

Before you move on, go ahead and explore the repository. You've already seen the **Source** page, but check out the **Commits**, **Branches**, and **Settings** pages.

---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).
>>>>>>> 45177700de079d5fa87de91239d17c4ee89cc596
